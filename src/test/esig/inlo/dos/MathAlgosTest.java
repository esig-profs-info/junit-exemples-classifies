package esig.inlo.dos;

import org.junit.Test;


import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import static esig.inlo.MathAlgos.factorielle;

/**
 * Une version que nous considérons "pro".
 */
public class MathAlgosTest {

    @Test
    public void testFactorielle_0() throws Exception {
        testerFactorielle(0, 1);
    }

    @Test
    public void testFactorielle_1() throws Exception {
        testerFactorielle(1, 1);
    }

    @Test
    public void testFactorielle_negatif() throws Exception {
        // Arrange
        final int n = -1;
        try {
            // Act
            factorielle(n);
            // Assert A
            fail("ArithmeticExcpetion should be thrown");
        } catch(ArithmeticException e) {
            // Assert B
            assertThat(e.getMessage(), is("La factorielle n'est pas définie pour les nombres négatifs. Reçu: " + n));
        }
    }

    @Test
    public void testFactorielle_2() throws Exception {
        testerFactorielle(2, 2);
    }

    @Test
    public void testFactorielle_3() throws Exception {
        testerFactorielle(3, 6);
    }

    @Test
    public void testFactorielle_10() throws Exception {
        testerFactorielle(10, 3628800);
    }

    @Test
    public void testFactorielle_laPlusGrandePossible() throws Exception {
        testerFactorielle(12, 479001600);
    }

    /**
     * 13! vaut 6227020800 ce qui est plus grand que {@link Integer#MAX_VALUE}.
     * Il aurait fallu utiliser {@code long} plutôt que {@code int} mais de toute manière il faut gérer ce débordement.
     */
    @Test
    public void testFactorielle_tropGrande() throws Exception {
        final int n = 13;
        try {
            factorielle(n);
            fail("ArithmeticExcpetion should be thrown");
        } catch(ArithmeticException e) {
            assertThat(e.getMessage(), is("Dépassement de capacité. n=" + n));
        }
    }

    private void testerFactorielle(int n, int resultatAttendu) {
        assertThat(factorielle(n), is(resultatAttendu));
    }


}