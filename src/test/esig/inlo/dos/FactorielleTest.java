package esig.inlo.dos;

import esig.inlo.MathAlgos;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Une version que nous considérons "pro".
 */
public class FactorielleTest {


    @Test
    public void testFactorielleDe0() throws Exception {
        assertEquals(1, MathAlgos.factorielle(0));
    }

    @Test
    public void testFactorielleDe1() {
        // Arrange: rien à faire
        // Act: on appelle le code à tester
        int resultat = MathAlgos.factorielle(1);
        // Assert: on vérifie le résultat
        assertEquals(1, resultat);
    }

    @Test
    public void testFactorielleDe2() throws Exception {
        assertEquals(2, MathAlgos.factorielle(2));
    }

    @Test
    public void testFactorielleDe3() throws Exception {
        assertEquals(6, MathAlgos.factorielle(3));
    }

    @Test
    public void testFactorielleDe10() throws Exception {
        assertEquals(3628800, MathAlgos.factorielle(10));
    }

    @Test(expected=ArithmeticException.class)
    public void testFactorielle_argumentNegatif1() throws Exception {
        MathAlgos.factorielle(-1);
    }

    @Test
    public void testFactorielle_argumentNegatif10() throws Exception {
        try {
            MathAlgos.factorielle(-10);
            fail("ArithmeticException expected");
        } catch (ArithmeticException ae) {
            // test OK
        }
    }

    /**
     * La factorielle de 12 est la plus grande valeur que nous pouvons calculer
     * sans dépasser la capacité du type de données <code>int</code>.
     * @throws Exception
     * @see Integer#MAX_VALUE
     */
    @Test
    public void testFactorielle_nombreMax() throws Exception {
        assertEquals(479001600, MathAlgos.factorielle(12));
    }

    /**
     * La factorielle de 13 dépasser la capacité du type de données <code>int</code>.
     * @throws Exception
     * @see Integer#MAX_VALUE
     */
    @Test(expected = ArithmeticException.class)
    public void testFactorielle_tropGrandNombre() throws Exception {
        MathAlgos.factorielle(13);
    }
}