package esig.inlo;

/**
 * Classe contenant des algorithmes mathématiques divers.
 * Created by mirko on 20.11.17.
 */
public class MathAlgos {

    public static int factorielle(int n) {
        if (n < 0) {
            throw new ArithmeticException("La factorielle n'est pas définie pour les nombres négatifs. Reçu: " + n);
        }
        if (n > 12) {
            throw new ArithmeticException("Dépassement de capacité. n=" + n);
        }
        int resultat = 1;
        for(int i = 2; i <= n; i++) {
            resultat *= i;
        }
        return resultat;
    }

    /* Un implémentation utilisant la récursion.
    public static int factorielleRecursive(int n) {
        if (n < 0) {
            throw new ArithmeticException();
        }
        return n == 0 ? 1 : n * factorielleRecursive(n - 1);
    }
    */

}
