package esig.inlo.donts;

import esig.inlo.MathAlgos;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by QUINLANPATRICK-ESIG on 29.11.2017.
 */
public class FactorielleTests {

    @Test
    public static void testFactorielleDe2() throws Exception {
        assertEquals(2, MathAlgos.factorielle(2));
    }

    @Test
    public void testFactorielleDe3(int n) throws Exception {
        assertEquals(6, MathAlgos.factorielle(n));
    }

    @Test
    public int testFactorielleDe10() throws Exception {
        int resultat = MathAlgos.factorielle(10);
        assertEquals(3628800, resultat);
        return resultat;
    }

}
