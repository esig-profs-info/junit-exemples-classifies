package esig.inlo.donts;

import esig.inlo.MathAlgos;
import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Created by mirko on 20.11.17.
 */
public class MathAlgosTest {

    @Test
    public void testFactorielle() throws Exception {
        int n = MathAlgos.factorielle(3);
        Assert.assertEquals(3, 3);
    }


    @Test
    public void testFactorielle3() throws Exception {
        Assert.assertEquals(MathAlgos.factorielle(3), 3);
    }


    @Test
    public void testFactorielle2() throws Exception {
        int n = 2;
        // calcul de la factorielle
        int resultat = 1;
        for(int i = 2; i <= n; i++) {
            resultat *= i;
        }
        Assert.assertEquals(2, resultat);
    }

}
