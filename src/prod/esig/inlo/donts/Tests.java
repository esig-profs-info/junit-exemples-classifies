package esig.inlo.donts;

import esig.inlo.MathAlgos;
import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.*;

/**
 * Created by mirko on 20.11.17.
 */
public class Tests {

    @Test
    public void name() throws Exception {
        TestCase.assertEquals(MathAlgos.factorielle(3), 6);
    }

    @Test
    public void test3(){
        System.out.println(MathAlgos.factorielle(5));
    }

    @Test
    public void test1(){
        System.out.println(MathAlgos.factorielle(1));
        System.out.println("Devrait être 1.0");
    }

    @Test
    public void test2(){
        MathAlgos.factorielle(5);
        Assert.assertEquals(2, MathAlgos.factorielle(2));
    }

    public static int quotient(int numerateur, int denominateur) {
        return numerateur/denominateur;

    }

    @Test (expected = Exception.class)
    public void TestNombreNegatif() throws Exception {
        double resultat = MathAlgos.factorielle(-1);
        TestCase.assertEquals(-1.0, resultat);
    }
}
